﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerAttack : MonoBehaviour
{

    private float attackTimer;
    public Camera cam;
    public GameObject PlayerHand;
    //public Weapon myWeapon;
    public float attackCooldown;
    
    void Start()
    {
    }

    void Update()
    {
        attackTimer += Time.deltaTime;
        if (Input.GetMouseButtonUp(0) && attackTimer >= attackCooldown)
        {
            DoAttack();
        }
    }

    private void DoAttack()
    {
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            if (PlayerHand.GetComponent<playerGrab>().hasWeapon != null)
            {
                if (hit.collider.tag == "Enemy")
                {
                    enemyHealth eHealth = hit.collider.GetComponent<enemyHealth>();
                    eHealth.takeDamage(25f);
                }
            }
        }
    }


}
