﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGrounded : MonoBehaviour
{
    void OnTriggerStay(Collider other)
    {
        PlayerMove myParent = transform.parent.GetComponent<PlayerMove>();
        myParent.grounded = true;

    }
}
