﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
public class PlayerMove : MonoBehaviour

{


    public Rigidbody rb;
    public CapsuleCollider c_collider;
    public Camera cam;
    public BoxCollider GroundCollider;
    public float moveSpeed = 10.0f;
    public float moveSpeedDef = 10.0f;
    public float gravity = 10.0f;
    public float maxVelocityChange = 10.0f;
    public bool canJump = true;
    public float jumpHeight = 2.0f;
    public bool grounded = false;
    private float speed;
    public float sprintMultiplier;
    public KeyCode sprintKey;
    public bool canMove;

    //Crouch
    public bool Crouch;
    public KeyCode crouchKey;
    public float crouchDistance = 0.75f;
    public float finalSize = 1.93f;
    public float moveMultiplier = 3;


    void Awake()
    {
        rb.freezeRotation = true;
        rb.useGravity = false;
        rb = GetComponent<Rigidbody>();
        c_collider = GetComponent<CapsuleCollider>();
        
    }

    public void Update()
    {
        Vector3 center = c_collider.center;
        Vector3 centerb = GroundCollider.center;
        Vector3 camP = cam.transform.position;
        if (Input.GetKeyDown(crouchKey) && Crouch == false)
        {
            moveSpeedDef = moveSpeedDef / moveMultiplier;
            center.y -= crouchDistance * 0.5f;
            c_collider.center = center;
            c_collider.height -= crouchDistance;
            camP.y -= crouchDistance * 3;
            cam.transform.position = camP;
            Crouch = true;
        }
        else if(Input.GetKeyUp(crouchKey))
        {
            moveSpeedDef = moveSpeedDef * moveMultiplier;            
            c_collider.height += crouchDistance;
            center.y += crouchDistance * 0.5f;
            c_collider.center = center;
            camP.y += crouchDistance * 3;
            cam.transform.position = camP;


            Crouch = false;
            
        }
        else
        {
            //Get Back to same position?
            Crouch = false;
        }
    }




    
    void FixedUpdate()
    {
        if (canMove == true)
        {
            moveSpeed = moveSpeedDef;
            canJump = true;
        }
        else if (canMove == false)
        {
            moveSpeed = 0f;
            canJump = false;
        }
        if (Input.GetKey(sprintKey))
        {
            speed = moveSpeed * sprintMultiplier;
        }
        else
        {
            speed = moveSpeed;
        }
        // Calculate how fast we should be moving
        Vector3 targetVelocity = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        targetVelocity = transform.TransformDirection(targetVelocity);
        targetVelocity *= speed;

        // Apply a force that attempts to reach our target velocity
        Vector3 velocity = rb.velocity;
        Vector3 velocityChange = (targetVelocity - velocity);
        velocityChange.x = Mathf.Clamp(velocityChange.x, -maxVelocityChange, maxVelocityChange);
        velocityChange.z = Mathf.Clamp(velocityChange.z, -maxVelocityChange, maxVelocityChange);
        velocityChange.y = 0;
        rb.AddForce(velocityChange, ForceMode.VelocityChange);
        if (grounded)
        {
            // Jump
            if (canJump && Input.GetButton("Jump"))
            {
                rb.velocity = new Vector3(velocity.x, CalculateJumpVerticalSpeed(), velocity.z);
            }
        }

        // We apply gravity manually for more tuning control
        rb.AddForce(new Vector3(0, -gravity * rb.mass, 0));

        grounded = false;
    }

    float CalculateJumpVerticalSpeed()
    {
        // From the jump height and gravity we deduce the upwards speed 
        // for the character to reach at the apex.
        return Mathf.Sqrt(2 * jumpHeight * gravity);
    }

}