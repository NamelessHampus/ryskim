﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerHit : MonoBehaviour
{
    private GameObject weapon;

    // Start is called before the first frame update
    void Start()
    {
    
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (GetComponent<playerGrab>().hasWeapon != null)
            {
                weapon = GetComponent<playerGrab>().weapon;
                var rot = weapon.transform.localRotation.eulerAngles; //get the angles
                rot.Set(0f, weapon.GetComponent<handRotation>().rotValue, 90f); //set the angles
                weapon.transform.localRotation = Quaternion.Euler(rot); //update the transform
                weapon.transform.localPosition = new Vector3(0, 0, 0);
            }

        }
        if (Input.GetMouseButtonUp(0))
        {
            if (GetComponent<playerGrab>().hasWeapon != null)
            {
                weapon = GetComponent<playerGrab>().weapon;
                var rot = weapon.transform.localRotation.eulerAngles; //get the angles
                rot.Set(0f, weapon.GetComponent<handRotation>().rotValue, 0f); //set the angles
                weapon.transform.localRotation = Quaternion.Euler(rot); //update the transform
                weapon.transform.localPosition = new Vector3(0, 0, 0);
            }
        }

    }
}
