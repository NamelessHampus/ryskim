﻿using UnityEngine;
using System.Collections;

public class playerGrab : MonoBehaviour
{
    public GameObject hasWeapon = null;   
    public GameObject playerCamera; //this is a reference to the main camera (drag & drop)
    public GameObject playerHand;
    private Rigidbody rb;
    public GameObject weapon;
    private Collider col;

    // Update is called once per frame
    void Update()
    {
        RaycastHit hitInfo; //a structure to hold hit information (gameobject we hit, position of the "impact" etc.)
        Ray r = new Ray(playerCamera.transform.position, playerCamera.transform.forward); //A ray starting from the camera, going forward

        //if we hit something
        if (Physics.Raycast(r, out hitInfo))
        {
            //if it is tagged as a weapon
            if (hitInfo.transform.CompareTag("Weapon"))
            {
                //if the user presses E
                if (Input.GetKeyDown(KeyCode.E))
                {
                    if (hasWeapon == null)
                    {
                        weapon = hitInfo.transform.gameObject;
                        rb = weapon.GetComponent<Rigidbody>();
                        col = weapon.GetComponent<Collider>();
                        weapon.gameObject.transform.parent = playerHand.transform;
                        rb.constraints = RigidbodyConstraints.FreezePosition | RigidbodyConstraints.FreezeRotation;
                        col.enabled = false;
                        weapon.gameObject.transform.localPosition = new Vector3(0, 0, 0);
                        weapon.gameObject.transform.localRotation = Quaternion.Euler(0, weapon.GetComponent<handRotation>().rotValue, 0);
                        hasWeapon = weapon;
                    }
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            if (rb != null && weapon != null)
            {
                rb.constraints = RigidbodyConstraints.None;
                col.enabled = true;
                weapon.transform.parent = null;
                weapon = null;
                hasWeapon = null;
            }
        }
    }
}
