﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HammerSplit : MonoBehaviour
{
    public GameObject destroyedVersion;

    void Update()
    {
        if (Input.GetMouseButtonDown(0)) {
            Instantiate(destroyedVersion, transform.position, transform.rotation);
            Destroy(gameObject);

        }
    }
}
