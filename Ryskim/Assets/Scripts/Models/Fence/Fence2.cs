﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fence2 : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        FenceController myParent = transform.parent.GetComponent<FenceController>();
        gameObject.GetComponent<MeshRenderer>().enabled = myParent.ShowVert2;
    }
}
